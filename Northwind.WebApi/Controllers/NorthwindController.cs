using Northwind.WebApi.Model;
using Microsoft.AspNetCore.Mvc;
using Northwind.WebApi.DbContextNamespace;

namespace app.Controllers;

[ApiController]
[Route("[controller]")]
public class NorthwindController : ControllerBase
{

  // Clients

  // Route permettant de retourner la liste des clients
  [HttpGet("customer/list")]
  public IEnumerable<Customer> Get()
  {
    using (var db = new NorthwindDbContext())
    {
      return db.Customers.ToList();
    }
  }

  // Route permettant de retourner un client par son identifiant
  [HttpGet("customer/{id}")]
  public Customer Get(int id)
  {
    using (var db = new NorthwindDbContext())
    {
      return db.Customers.Find(id);
    }
  }

  // Route permettant d'ajouter un client
  [HttpPost("customer/add")]
  public Customer Post([FromBody] Customer customer)
  {
    using (var db = new NorthwindDbContext())
    {
      db.Customers.Add(customer);
      db.SaveChanges();
      return customer;
    }
  }

  // Route permettant de retourner la liste des commandes d'un client
  [HttpGet("customer/{id}/orders")]
  public IEnumerable<Order> GetCategory(int id)
  {
    using (var db = new NorthwindDbContext())
    {
      return db.Orders.Where(p => p.CustomerId == id).ToList();
    }
  }
}


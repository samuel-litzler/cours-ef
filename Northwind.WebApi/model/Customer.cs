﻿using System;
using System.Collections.Generic;
// Deux imports qui viennent ajouter des espace de nom pour la définition des limites de nos propriétés
// On retrouvera la même chose pour toutes nos classes
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Northwind.WebApi.Model;

public partial class Customer
{
    [Key]
    [Column("CustomerID")]
    public int CustomerId { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? CustomerName { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? ContactName { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? Address { get; set; }

    [StringLength(20)]
    [Unicode(false)]
    public string? City { get; set; }

    [StringLength(10)]
    [Unicode(false)]
    public string? PostalCode { get; set; }

    [StringLength(5)]
    [Unicode(false)]
    public string? CodeInsee { get; set; }

    [StringLength(15)]
    [Unicode(false)]
    public string? Country { get; set; }

    // Propriété qui va définir la relation entre Customer et Order
    // L'importance de la relation sera défini dans Northmind.cs
    [InverseProperty("Customer")]
    public virtual ICollection<Order> Orders { get; set; } = new List<Order>();
}

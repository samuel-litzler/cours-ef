﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace consoleApp.Migrations
{
    /// <inheritdoc />
    public partial class Northwind_code_fist_update : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CodeInsee",
                table: "Customers",
                type: "varchar(5)",
                unicode: false,
                maxLength: 5,
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CodeInsee",
                table: "Customers");
        }
    }
}

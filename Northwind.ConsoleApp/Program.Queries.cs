using EFCore;

namespace EFCore;

// Exercice 2
class ProgramQueries {
  public static void Query1() {
    using Northwind db = new();

    Console.WriteLine("Veuiller entre un prix :");

    String s = Console.ReadLine(); // Récupération saisie
    //TODO ajout si saisie null

    List<Product> p = db.Products.Where(p => p.Price > decimal.Parse(s)).ToList();

    // afficher pour chaques produits trouvés les informations du nom/prix/quantité et catégorie
    foreach (Product p1 in p) {
      db.Entry(p1).Reference(p => p.Category).Load();
      Console.WriteLine(p1.ProductName + " - (" + p1.Price + ") " + p1.Unit + " - " + p1.Category.CategoryName);
    }
  }
}

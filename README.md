# Cours Entity Framework

Sommaire : 
- [Notes du cours](#notes-du-cours)
- [Structurer un projet](#structurer-un-projet-net-core)
- [Etapes d'installation EF Tools](#etapes-dinstallation-defcore-tool)
- [Etape du cours](#etapes-du-cours)
- [Commandes utiles](#commandes-utiles)
- [Consignes du cours](#consignes-du-cours)
- [Problèmes rencontrés](#problèmes-rencontrés)


## **Notes du cours**
### **Mots clés**
- **SDK** : Software Development Kit
- **STS** : Standard Term Support
- **SGDBR** : Système Gestion Base de Données Relationnel

### **Questions / Réponses**
#### **A quoi sert le fichier .csproj ?**
> Il permet de gérer les dépendances du projet.

#### **Qu’est-ce que c’est Entity Framework ?**
> Entity Framework est l'ORM utilisé par la technologie Microsoft .NET.

#### **Qu’est-ce que c’est .NET ?**
> C'est le framework de microsoft qui apport énormément de classes pour le bon fonctionnement et la créationd d'une application .NET.

#### **Qu’est-ce que c’est EF Tools ?**
> EF Tools va nous permettre de faire des migrations.

#### **Qu’est-ce que c’est [Nuget](https://www.nuget.org/) ?**
> Nugget est le gestionnaire de package pour .NET.

#### **Qu’est-ce que c’est ASP ?**
> Active Server Pages: c'est une plateforme pour créer des sites web dynamiques server side. On utilise de l'HTML et du code (C#). Anciennement utilisé avec VB, à bannir.

#### **Qu’est-ce que c’est ASP.NET Web Forms ?**
> ASP.NET Web Forms c'est du drag and drop pour créer des sites web.

#### **Qu’est-ce que c’est WCF ?**
> Windows Communication Foundation : Permet au dev de créer des web service en SOAP (à bannir) ou en REST

#### **Qu’est-ce que c’est ASP.NET MVC (2009) ?**
> Première mise en place du pattern "Separation of Concerns". La couche model stocke les données temporairement.

#### **Qu’est-ce que c’est ASP.NET API ?**
> Création de web service avec http (REST)
#### **Qu’est-ce que c’est ASP.NET SignalR ?**
> Permet au dev de mettre en place de la communication en temps réél, c'est une couche d'abstraction sur des technologies plus complexes (WebSockets).
#### **Qu’est-ce que c’est ASP.NET Core ?**
> Combine les implementations les plus modernes du .NET (MVC, Web API, SignalR, Razor Pages, gRPC, Blazor), On a aussi enfin du cross platform !


****
## **Structurer un projet .NET CORE :** 
EFCore.Commun :
> Il s'agit d'une bibliotheque de classe pour les types communs tels que : les interfaces, enums, classes, structs. Il peut etre reutiliser par plusieurs projets.

EFCore.Common.EntityModels
> une classlib qui contient les modele entite d'EF Core

EFCore. Common.DataContext:
> c'est une classlib qui contient le context EF Core (DbContext) avec les dependances gpecifiques au provider de bdd

EFCore.Web:
> Projet de site web statique, ou un projet razor pages

EFCore.Razor.Component:
> Une classlib pour des Razor Pages utilisées au sein de plusieurs projets

EFCore.Mvc:
> Pour la creation d'app web avec le pattern MVC.

EFCore.WebApi:
> Contient un projet ASP.NET Core d'API Rest. Peut etre utilisé avec un framework JS ou Blazor.

EFCore.BlazorServer:
> Un projet ASP.NET Core Blazor Server

****
## **Etapes d'installation d'EFCore Tool**
[lien documentation](https://learn.microsoft.com/fr-fr/ef/core/cli/dotnet)
- Afficher les possibilités d'installation : `dotnet tool list --global`
- Désinstaller une ancienne version : `dotnet tool uninstall --global dotnet-ef`
- Installer la dernière version : `dotnet tool install --global dotnet-ef`
- Ajout du package Design.
```sh
dotnet add package Microsoft.EntityFrameworkCore.Design --version 7.0.8
```
- Génération de la structure des schémas depuis la base de données : 
    - VERSION SQLLITE
    ```sh
    dotnet ef dbcontext scaffold "Filename=Northwind.db" Microsoft.EntityFrameworkCore.Sqlserver --table Categories --table Products --output-dir AutoGeneratedModels --namespace EFCore.AutoGenerated --data-annotations --context Northwind
    ```
    - VERSION SQLSERVER
    ```sh
    dotnet ef dbcontext scaffold "Data Source=(local);Initial Catalog=Northwind;Integrated Security=True;TrustServerCertificate=True" Microsoft.EntityFrameworkCore.SqlServer --table Categories --table Products --table OrderDetails --table Suppliers --table Orders --table Shippers --table Employees --table Customers --table Employees --output-dir AutogeneratedModels --namespace EFCore.AutoGenerated --data-annotations --context Northwind
    ```

---
## **Etapes du cours**
- Installation d'un provider de base de données
- Connexion à la DB, il faut installer le bon package (SQLServer)
```
dotnet add package Microsoft.EntityFrameworkCore.SqlServer -v 7.0.8
```
- Utilisation du package Design, on va utiliser les étapes de [dotnet tools](#quest-ce-que-cest-ef-tools) pour générer le modèle
- Pour générer la db via le modèle on va utiliser cette commande : 
  - Dans le Northwind.cs j'ai modifié le nom de la base de donnée (Northwind_code_first) pour créer la nouvelle : `dotnet ef migrations add Northwind_code_fist_init`
  - Ensuite il suffit de mettre à jour les changements : `dotnet ef database update`
  - Puis j'ai ajouté une colonne dans Customer : CodeInsee et recréé une autre migration : `dotnet ef migrations add Northwind_code_fist_update` puis en exécutant de nouveau la commande précédente pour mettre à jours. En vérifiant sur SSMS la nouvelle colonne à bien été ajoutée.
- Faire une solution avec plusieurs projets [Voir l'architecture](#structurer-un-projet-net-core)
    - on va faire une API qui permet d'ajouter/modifier en base de données, la génération se ferra grâce à WebApi
---
## **Commandes utiles** 
- Créer une application : `dotnet new [nomModele] -o [nomApp] -f [version]`
- Build l'application pour vérifier le code : `dotnet build`
- Build et lance l'application : `dotnet run`
- connaître la liste de ce que dotnet peut générer `dotnet new list` 
- Recharger le csproj : `dotnet restore`
- Ajouter un package : `dotnet add package [nom du paquet]  -v [version]`
- Supprimer un package : `dotnet remove package [nom du paquet]`

---

## **Consignes du cours**

Objectif du cours : 
- On va utiliser la db NorthWind qui vient du tuto officiel de MS
- Le but est de gérer cette DB au sein d’une application de notre choix.

Exercice 1 : 

Pour le fichier Category.cs et Product.cs
- creer trois des quatre properties (toute sauf Image)
- penser à la PK
- gerer ou penser a une propriété qui gere la relation one-to-many avec la table Products

Exercice 2 : 
Dans le fichier Program.Queries.cs creez une methode qui permet d'effectuer des requetes sur la table product :
- creez une instance de la class Northwind
- utilisez un prompt pour demander un prix a l'utilisateur en console
- creez une requete pour les produit dont le prix est superieur au prix entre par l'utilisateur (avec LINQ)
- parcourez le resultat de la requete et afficher le nom, le prix et le stock (pour chaque prduit donc)

---

## **Problèmes rencontrés**
- compatibilité nuget 

De lundi à mardi j'ai eu un soucis à propos de nuget, je ne pouvais pas installer de package n'y build correctement.
J'ai enfin trouvé la réponse à mon problème mardi soir ici 